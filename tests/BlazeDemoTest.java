import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BlazeDemoTest {

final String CHROMEDRIVER_LOCATION = "/Users/harman/Desktop/chromedriver";
	
	//website we want to test
	final String URL_TO_TEST = "http://blazedemo.com";

	//global variables
	
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		//1. setup  selenium and your web driver
		System.setProperty("webdriver.chrome.driver",CHROMEDRIVER_LOCATION);
		driver = new ChromeDriver();
		
		//2. go to the website you want to test
		driver.get(URL_TO_TEST);
		System.out.println("This is setup");
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.close();
	}

	@Test
	public void numberOfDepartureCitiesTest() {
		List<WebElement> departureCities = driver.findElements(By.cssSelector("h2+select option"));
		System.out.println("Number of departure cities on page: "+departureCities.size());
		//fail("Not yet implemented");
	}
	
	@Test
	public void virginAmericaFlightDetails() {
		WebElement button = driver.findElement(By.cssSelector("div>input"));
		button.click();
		List<WebElement> flightList = driver.findElements(By.cssSelector("h3>table+tbody tr"));
		System.out.println(flightList.size());
//		for(int i=0; i < flightList.size(); i++)
//	    {
//	    
//	    }
		
		
		
	}

}
