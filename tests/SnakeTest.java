import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {
	
	Snake snake1;
	Snake snake2;

	@Before
	public void setUp() throws Exception {
		//create two snakes
		
		snake1 = new Snake("Peter", 10, "coffee");
		snake2 = new Snake("Takis", 80, "vegetables");
		//System.out.println(snake1.isHealthy());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void snakeIsHealthyTest() {
		
		//assertTrue("Snake is healthy",snake2.isHealthy());
		
		//returns false if snake doesnot have favourite food as vegetables
		assertEquals(false, snake1.isHealthy());
		
		//returns true if favourite food is vegetables
		assertEquals(true, snake2.isHealthy());
		//fail("Not yet implemented");
	}
	
	@Test
	public void fitsInCage() {
		int cageLength = 20;
		assertEquals(true, snake1.fitsInCage(cageLength));
		
		
	}
	

}
